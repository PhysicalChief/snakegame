// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Food.h"
#include "FoodSpeedUpClass.generated.h"

/**
 * 
 */
UCLASS()
class SNAKEGAME_API AFoodSpeedUpClass : public AFood
{
	GENERATED_BODY()
	
protected:

public:
	UPROPERTY(EditDefaultsOnly)
		float upValue = 0.05;

	virtual void Interact(AActor* interactor, bool bIsHead) override;
};
