// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "WallClass.h"
#include "BadWall.generated.h"

/**
 * 
 */
UCLASS()
class SNAKEGAME_API ABadWall : public AWallClass
{
	GENERATED_BODY()
		
public:
	virtual void Interact(AActor* interactor, bool bIsHead) override;
	
};
