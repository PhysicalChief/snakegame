// Fill out your copyright notice in the Description page of Project Settings.


#include "WallClass.h"
#include "SnakeBaseActor.h"
#include "Food.h"

// Sets default values
AWallClass::AWallClass()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AWallClass::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void AWallClass::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}


void AWallClass::Interact(AActor* interactor, bool bIsHead)
{
	if (bIsHead) {
		auto snake = Cast<ASnakeBaseActor>(interactor);
		if (IsValid(snake)) {
			snake->BackStep();
		}
	}
	else {
		auto food = Cast<AFood>(interactor);
		if (IsValid(food)) {
			food->Replace();
		}
	}
}

