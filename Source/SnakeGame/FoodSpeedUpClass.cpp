// Fill out your copyright notice in the Description page of Project Settings.


#include "FoodSpeedUpClass.h"
#include "SnakeBaseActor.h"

void AFoodSpeedUpClass::Interact(AActor* interactor, bool bIsHead)
{
	if (bIsHead) {
		auto snake = Cast<ASnakeBaseActor>(interactor);
		if (IsValid(snake)) {
			snake->SpeedUp(upValue);
			this->Destroy();
		}
	}
}