// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SnakeBaseActor.generated.h"

class ASnakeElementBase;

UENUM()
enum class EMovementDirection
{
	UP,
	DOWN,
	LEFT,
	RIGHT
};

UCLASS()
class SNAKEGAME_API ASnakeBaseActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASnakeBaseActor();

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ASnakeElementBase> SnakeElementBaseClass;

	UPROPERTY(EditDefaultsOnly)
		float elementSize = 100.0f;

	UPROPERTY(EditDefaultsOnly)
		float moveOffset = 50;

	UPROPERTY()
		TArray<ASnakeElementBase*> SnakeElements;

	UPROPERTY()
		EMovementDirection lastMoveDirection;

	UPROPERTY(EditDefaultsOnly)
		float movementSpeed;

	bool isMoveable = true;
	FVector prefLastElemPosition1;
	FVector prefLastElemPosition2;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	UFUNCTION(BlueprintCallable)
		void AddSnakeElement(int elementsNum = 1);

	UFUNCTION(BlueprintCallable)
		void Move();

	UFUNCTION()
		void SnakeElementOverlap(ASnakeElementBase* overlappedBlock, AActor* otherActor);

	UFUNCTION()
		void BackStep();

	UFUNCTION()
		void SpeedUp(float bonusSpeed);

	UFUNCTION()
		void SpeedDown(float debufSpeed);
};
