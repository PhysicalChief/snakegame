// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"
#include "SnakeBaseActor.h"

// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	MeshComponent->SetCollisionResponseToAllChannels(ECR_Overlap);

}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFood::Interact(AActor* interactor, bool bIsHead)
{
	if (bIsHead) {
		auto snake = Cast<ASnakeBaseActor>(interactor);
		if (IsValid(snake)) {
			snake->AddSnakeElement();
			this->Replace();
		}
	}
}

void AFood::Replace()
{
	FVector newPosition = FVector(FMath::FRandRange(minXposFood, maxXposFood), FMath::FRandRange(minYposFood, maxYposFood), 30);
	this->SetActorLocation(newPosition);
}

