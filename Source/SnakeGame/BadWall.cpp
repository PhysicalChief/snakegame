// Fill out your copyright notice in the Description page of Project Settings.


#include "BadWall.h"
#include "SnakeBaseActor.h"
#include "Food.h"

void ABadWall::Interact(AActor* interactor, bool bIsHead)
{
	if (bIsHead) {
		auto snake = Cast<ASnakeBaseActor>(interactor);
		if (IsValid(snake)) {
			snake->Destroy();
		}
	}
	else {
		auto food = Cast<AFood>(interactor);
		if (IsValid(food)) {
			food->Replace();
		}
	}
}