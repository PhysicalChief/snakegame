// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeElementBase.h"
#include "SnakeBaseActor.h"
#include "Food.h"
 

// Sets default values
ASnakeElementBase::ASnakeElementBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	MeshComponent->SetCollisionResponseToAllChannels(ECR_Overlap);

}

// Called when the game starts or when spawned
void ASnakeElementBase::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASnakeElementBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASnakeElementBase::SetFirstElementType_Implementation()
{
	MeshComponent->OnComponentBeginOverlap.AddDynamic(this, &ASnakeElementBase::HandleBeginOverlap);
}

void ASnakeElementBase::Interact(AActor* interactor, bool bIsHead)
{
	if (bIsHead) {
		auto snake = Cast<ASnakeBaseActor>(interactor);
		if (IsValid(snake)) {
			snake->Destroy();
		}
	}
	else {
		auto food = Cast<AFood>(interactor);
		if (IsValid(food)) {
			food->Replace();
		}
	}
}

void ASnakeElementBase::HandleBeginOverlap(UPrimitiveComponent* overlapComponent, 
											AActor* otherActor, 
											UPrimitiveComponent* otherComponent, 
											int32 otherBodyIndex, 
											bool bFromSweep, 
											const FHitResult& sweepResult)
{
	if (IsValid(SnakeOwner)) {
		SnakeOwner->SnakeElementOverlap(this, otherActor);
	}
}


void ASnakeElementBase::ToggleCollision()
{
	if (MeshComponent->GetCollisionEnabled() == ECollisionEnabled::NoCollision) {
		MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	}
	else {
		MeshComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	}
	
}

