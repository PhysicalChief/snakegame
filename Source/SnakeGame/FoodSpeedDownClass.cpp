// Fill out your copyright notice in the Description page of Project Settings.


#include "FoodSpeedDownClass.h"
#include "SnakeBaseActor.h"

void AFoodSpeedDownClass::Interact(AActor* interactor, bool bIsHead)
{
	if (bIsHead) {
		auto snake = Cast<ASnakeBaseActor>(interactor);
		if (IsValid(snake)) {
			snake->SpeedDown(downValue);
			this->Destroy();
		}
	}
}

