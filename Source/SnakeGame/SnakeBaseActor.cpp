// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBaseActor.h"
#include "SnakeElementBase.h"
#include "Interactable.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"

// Sets default values
ASnakeBaseActor::ASnakeBaseActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	elementSize = 60;
	lastMoveDirection = EMovementDirection::DOWN;
	movementSpeed = 0.5;

}

// Called when the game starts or when spawned
void ASnakeBaseActor::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(movementSpeed);
	AddSnakeElement(5);
}

// Called every frame
void ASnakeBaseActor::Tick(float DeltaTime)
{
	if (isMoveable) {
		Super::Tick(DeltaTime);
		Move();
	}
}

void ASnakeBaseActor::AddSnakeElement(int elementsNum)
{
	for (int i = 0; i < elementsNum; i++) {
		FVector newLocation = FVector(SnakeElements.Num() * elementSize, 0, 0);
		FTransform transform = FTransform(newLocation);
		ASnakeElementBase* newSnakeElement = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementBaseClass, transform);
		newSnakeElement->SnakeOwner = this;
		int32 index = SnakeElements.Add(newSnakeElement);
		if (index == 0) {
			newSnakeElement->SetFirstElementType();
		}
	}
}

void ASnakeBaseActor::Move()
{
	FVector movementVector(0, 0, 0);
	float movementOffset = elementSize;
	switch (lastMoveDirection)
	{
	case EMovementDirection::UP:
		movementVector.X += movementOffset;
		break;
	case EMovementDirection::DOWN:
		movementVector.X -= movementOffset;
		break;
	case EMovementDirection::LEFT:
		movementVector.Y += movementOffset;
		break;
	case EMovementDirection::RIGHT:
		movementVector.Y -= movementOffset;
		break;
	default:
		break;
	}
	//AddActorWorldOffset(movementVector);

	SnakeElements[0]->ToggleCollision();
	prefLastElemPosition2 = prefLastElemPosition1;
	prefLastElemPosition1 = SnakeElements[SnakeElements.Num() - 1]->GetActorLocation();

	for (int i = SnakeElements.Num() - 1; i > 0; i--) {
		auto currentElement = SnakeElements[i];
		auto prefElement = SnakeElements[i - 1];
		FVector prefElementPosition = prefElement->GetActorLocation();
		currentElement->SetActorLocation(prefElementPosition);
	}
	SnakeElements[0]->AddActorWorldOffset(movementVector);
	SnakeElements[0]->ToggleCollision();

}

void ASnakeBaseActor::SnakeElementOverlap(ASnakeElementBase* overlappedElement, AActor* otherActor)
{
	if (IsValid(overlappedElement)) {
		int32 elementIndex;
		SnakeElements.Find(overlappedElement, elementIndex);
		bool bIsFirst = elementIndex == 0;
		IInteractable* interactableInterface = Cast<IInteractable>(otherActor);
		if (interactableInterface) {
			interactableInterface->Interact(this, bIsFirst);
		}
	}
}

void ASnakeBaseActor::BackStep()
{
	isMoveable = false;
	SnakeElements[0]->ToggleCollision();
	int32 nums = SnakeElements.Num();
	for (int i = 0; i <= nums - 3; i++) {
		auto current = SnakeElements[i];
		auto next = SnakeElements[i + 2];
		FVector nextElemPosition = next->GetActorLocation();
		current->SetActorLocation(nextElemPosition);
	}
	SnakeElements[nums - 1]->SetActorLocation(prefLastElemPosition2);
	SnakeElements[nums - 2]->SetActorLocation(prefLastElemPosition1);
	isMoveable = true;
	SnakeElements[0]->ToggleCollision();
}

//�������� �������� ������ ��� ��� ������������� ����������
void ASnakeBaseActor::SpeedUp(float bonusSpeed)
{
	movementSpeed -= bonusSpeed;
	SetActorTickInterval(movementSpeed);
}

void ASnakeBaseActor::SpeedDown(float debufSpeed)
{
	movementSpeed += debufSpeed;
	SetActorTickInterval(movementSpeed);
}

