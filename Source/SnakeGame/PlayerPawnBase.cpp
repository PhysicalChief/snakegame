// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerPawnBase.h"
#include "Engine/Classes/Camera/CameraComponent.h"
#include "SnakeBaseActor.h"


// Sets default values
APlayerPawnBase::APlayerPawnBase()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	PawnCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("PawnCamera"));
	RootComponent = PawnCamera;

}

void APlayerPawnBase::HandlerPlayerVerticalInput(float value)
{
	if (IsValid(SnakeActor)) {
		if (value > 0 && SnakeActor->lastMoveDirection != EMovementDirection::DOWN) {
			SnakeActor->lastMoveDirection = EMovementDirection::UP;
		}
		else if (value < 0 && SnakeActor->lastMoveDirection != EMovementDirection::UP) {
			SnakeActor->lastMoveDirection = EMovementDirection::DOWN;
		}
	}
}

void APlayerPawnBase::HandlerPlayerHorizontalInpput(float value)
{
	if (IsValid(SnakeActor)) {
		if (value > 0 && SnakeActor->lastMoveDirection != EMovementDirection::RIGHT) {
			SnakeActor->lastMoveDirection = EMovementDirection::LEFT;
		}
		else if (value < 0 && SnakeActor->lastMoveDirection != EMovementDirection::LEFT) {
			SnakeActor->lastMoveDirection = EMovementDirection::RIGHT;
		}
	}
}

// Called when the game starts or when spawned
void APlayerPawnBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorRotation(FRotator(-90, 0, 0));
	CreateSnakeActor();

}

// Called every frame
void APlayerPawnBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void APlayerPawnBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("vertical", this, &APlayerPawnBase::HandlerPlayerVerticalInput);
	PlayerInputComponent->BindAxis("horizontal", this, &APlayerPawnBase::HandlerPlayerHorizontalInpput);

}

void APlayerPawnBase::CreateSnakeActor()
{
	SnakeActor = GetWorld()->SpawnActor<ASnakeBaseActor>(SnakeActorClass, FTransform());

}

